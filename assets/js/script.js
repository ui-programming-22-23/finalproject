// get a handle to the canvas context
const canvas = document.getElementById("the_canvas")
// get 2D context for this canvas
const context = canvas.getContext("2d");

// gets the image 
let playerCharacter = new Image();
let enemy = new Image();
let backgroundImg = new Image();

// sets the image
playerCharacter.src = "assets/img/Knight(Right).png";
enemy.src = "assets/img/slime.png";
backgroundImg.src = "assets/img/level.1.png";

// method for accessing the button
let shopBtn = document.querySelector(".shop_btn");
let caveBtn = document.querySelector(".cave_btn");
let starBtn = document.querySelector(".star-btn");
let attackBtn = document.querySelector(".attack_btn");
let moonBtn = document.querySelector(".half-moon-btn");
let rightArrow = document.querySelector(".arrow-button-right");
let leftArrow = document.querySelector(".arrow-button-left");
let backArrow = document.querySelector(".back-button-doors");
let backArrowShop = document.querySelector(".back-button-shop");

// Not displaying buttons
starBtn.style.display = "none";
attackBtn.style.display = "none";
moonBtn.style.display = "none";
backArrow.style.display = "none";
backArrowShop.style.display = "none";
shopBtn.style.display = "none";
caveBtn.style.display = "none";

// keeps track of what character the user is on in the selection menu
let characterCount = 0;

// used to check if the game is started
let gameStart = false;

let playerShouldBeOnScreen = true;
let slimeShouldBeOnScreen = false;
let playerCanMove = false;

// gets the text of the characters class
let playerClass = document.querySelector("h1.class");

// GameObject holds positional information
function GameObject(spritesheet, x, y, width, height) {
    this.spritesheet = spritesheet;
    this.x = x;
    this.y = y;
    this.width = width;
    this.height = height;
}

// Default Player
let player = new GameObject(playerCharacter, 700, 200, 473, 546);
let background = new GameObject(backgroundImg, 0, 0, 1920, 1080);
let slime = new GameObject(enemy, 1300, 500, 135, 156);

let playersHealthBar = document.getElementById("playersHealthBar");
let enemiesHealthBar = document.getElementById("enemiesHealthBar");
let playersHealth = 100;
let enemiesHealth = 100;
let playersHealthScale = 20;
let enemieHealthScale = 20;

playersHealthBar.style.width = playersHealthScale + "%";
playersHealthBar.style.display = "none"
enemiesHealthBar.style.width = enemieHealthScale + "%";
enemiesHealthBar.style.display = "none"

// used to store players location before battling
let playersLastPosX = player.x;
let playerLastPosY = player.y;

// The GamerInput is an Object that holds the Current
// GamerInput (Left, Right, Up, Down, MouseClicks)
function GamerInput(input) {
    this.action = input; // Hold the current input as a string
}

// Default GamerInput is set to None
let gamerInput = new GamerInput("None"); //No Input


function input(event) {
    // Take Input from the Player
    // console.log("Input");
    console.log(event);
    console.log("Event type: " + event.type);
    // console.log("Keycode: " + event.keyCode);

    if (event.type === "keydown") {
        switch (event.keyCode) {
            case 37: // Left Arrow
                gamerInput = new GamerInput("Left");
                break; //Up key
            case 39: // Right Arrow
                gamerInput = new GamerInput("Right");
                break; //Down key
            default:
                gamerInput = new GamerInput("None"); //No Input
        }
    } else {
        gamerInput = new GamerInput("None");
    }
}


function update() {
    // console.log("Update");
    // Check Input
    if(playerCanMove === true)
    {
    if (gamerInput.action === "Up")
    {
        console.log("Move Up");
        player.y -= 4; // Move Player Up
    } 
    else if (gamerInput.action === "Left")
    {
        // checks what character the user chose
        if(characterCount === 0)
        {
            playerCharacter.src = "assets/img/Knight(Left).png";
        }
        else if (characterCount === 1)
        {
            playerCharacter.src = "assets/img/Barbarian(Left).png";
        }
        else if (characterCount === 2)
        {
            playerCharacter.src = "assets/img/Wizard(Left).png";
        }
        else if (characterCount === 3)
        {
            playerCharacter.src = "assets/img/CrossbowMan(Left).png";
        }
        console.log("Move Left");
        player.x -= 4; // Move Player Left
        checksForEncounter();
    } 
    else if (gamerInput.action === "Right")
    {
        if(characterCount === 0)
        {
            playerCharacter.src = "assets/img/Knight(Right).png";
        }
        else if (characterCount === 1)
        {
            playerCharacter.src = "assets/img/Barbarian(Right).png";
        }
        else if (characterCount === 2)
        {
            playerCharacter.src = "assets/img/Wizard(Right).png";
        }
        else if (characterCount === 3)
        {
            playerCharacter.src = "assets/img/CrossbowMan(Right).png";
        }
        console.log(player.x);
        player.x += 4; // Move Player Right
        checksForEncounter();
    }
    }   

    if (gameStart === false)
    {
        console.log(gameStart);
        if(characterCount === 0)
        {
            playerCharacter.src = "assets/img/Knight(Right).png";
            playerClass.textContent = "Knight";
            player = new GameObject(playerCharacter, 700, 200, 473, 546);
        }
        else if (characterCount === 1)
        {
            playerCharacter.src = "assets/img/Barbarian(Right).png";
            playerClass.textContent = "Barbarian";
            player = new GameObject(playerCharacter, 700, 200, 473, 546);
        }
        else if (characterCount === 2)
        {
            playerCharacter.src = "assets/img/Wizard(Right).png";
            playerClass.textContent = "Wizard";
            player = new GameObject(playerCharacter, 755, 120, 360, 637);
        }
        else if (characterCount === 3)
        {
            playerCharacter.src = "assets/img/CrossbowMan(Right).png";
            playerClass.textContent = "CrossbowMan";
            player = new GameObject(playerCharacter, 725, 200, 420, 555);
        }
    }

    // if the player reached the end of the cave
    if(player.x > 1880)
    {
        backgroundImg.src = "assets/img/doors.png";
        playerShouldBeOnScreen = false;
        playerCanMove = false;
        moonBtn.style.display = "block";
        starBtn.style.display = "block";
        backArrow.style.display = "block";
        player.x = 1800;
        if(characterCount === 0)
        {
            playerCharacter.src = "assets/img/Knight(Left).png";
        }
        else if (characterCount === 1)
        {
            playerCharacter.src = "assets/img/Barbarian(Left).png";
        }
        else if (characterCount === 2)
        {
            playerCharacter.src = "assets/img/Wizard(Left).png";
        }
        else if (characterCount === 3)
        {
            playerCharacter.src = "assets/img/CrossbowMan(Left).png";
        }
    }
    if(player.x < -100)
    {
        backInTown();
    }

}

function draw() {
    // Clear Canvas
    context.clearRect(0, 0, canvas.width, canvas.height);
    //console.log("Draw");
    //console.log(player);

    if(gameStart === true)
    {
        context.drawImage(background.spritesheet, 
            background.x,
            background.y,
            background.width,
            background.height);
    }  

    if(slimeShouldBeOnScreen === true)
    {
        context.drawImage(slime.spritesheet, 
            slime.x,
            slime.y,
            slime.width,
            slime.height);
    }
    if(playerShouldBeOnScreen === true)
    {
    context.drawImage(player.spritesheet, 
                      player.x,
                      player.y,
                      player.width,
                      player.height);
    }
}

function gameloop() {
    update();
    draw();
    window.requestAnimationFrame(gameloop);
}

// Handle Active Browser Tag Animation
window.requestAnimationFrame(gameloop);

// https://developer.mozilla.org/en-US/docs/Web/API/window/requestAnimationFrame

window.addEventListener('keydown', input);
// disable the second event listener if you want continuous movement
window.addEventListener('keyup', input);

function insideShop() {
    backgroundImg.src = "assets/img/insideShop.png";
    playerCharacter.src = "assets/img/Knight(Left).png";
    shopBtn.style.display = "none";
    caveBtn.style.display = "none";
    backArrowShop.style.display = "block";
}

function insideCave() {
    backgroundImg.src = "assets/img/insideCave.png";
    shopBtn.style.display = "none";
    caveBtn.style.display = "none";
    backArrow.style.display = "none";
    moonBtn.style.display = "none";
    starBtn.style.display = "none";
    playerShouldBeOnScreen = true;
    playerCanMove = true;
}

function backInTown() {
    backgroundImg.src = "assets/img/level.1.png";
    shopBtn.style.display = "block";
    caveBtn.style.display = "block";
    backArrowShop.style.display = "none";
    playerShouldBeOnScreen = false;
    playerCanMove = true;
    player.x = 0;
    if(characterCount === 0)
        {
            playerCharacter.src = "assets/img/Knight(Right).png";
        }
        else if (characterCount === 1)
        {
            playerCharacter.src = "assets/img/Barbarian(Right).png";
        }
        else if (characterCount === 2)
        {
            playerCharacter.src = "assets/img/Wizard(Right).png";
        }
        else if (characterCount === 3)
        {
            playerCharacter.src = "assets/img/CrossbowMan(Right).png";
        }
}

function next() {

    characterCount++;
    if(characterCount === 4)
    {
        characterCount = 0;
    }
}

function previous() {

    characterCount--;
    if (characterCount === -1)
    {
        characterCount = 3;
    }
}

function gameStarts(event) {

    event.preventDefault(); // prevent the page from reloading
    if(characterCount === 0)
        {
            playerCharacter.src = "assets/img/Knight(Right).png";
            playerClass.textContent = "Knight";
            player = new GameObject(playerCharacter, 0, 900, 135, 156);
        }
        else if (characterCount === 1)
        {
            playerCharacter.src = "assets/img/Barbarian(Right).png";
            playerClass.textContent = "Barbarian";
            player = new GameObject(playerCharacter, 0, 900, 135, 156);
        }
        else if (characterCount === 2)
        {
            playerCharacter.src = "assets/img/Wizard(Right).png";
            playerClass.textContent = "Wizard";
            player = new GameObject(playerCharacter, 0, 900, 103, 192);
        }
        else if (characterCount === 3)
        {
            playerCharacter.src = "assets/img/CrossbowMan(Right).png";
            playerClass.textContent = "CrossbowMan";
            player = new GameObject(playerCharacter, 0, 900, 120, 159);
        }


    gameStart = true;
    document.getElementById("starterForm").style.display = "none";
    document.getElementById("arrow-button-right").style.display = "none";
    document.getElementById("arrow-button-left").style.display = "none";
    document.getElementById("class").style.display = "none";
    shopBtn.style.display = "block";
    caveBtn.style.display = "block";
    playerShouldBeOnScreen = false;
    playerCanMove = false;

}

function win() {
    backgroundImg.src = "assets/img/youWin.png";
    moonBtn.style.display = "none";
    starBtn.style.display = "none";
    backArrow.style.display = "none";
}

function lose() {
    backgroundImg.src = "assets/img/gameOver.png";
    moonBtn.style.display = "none";
    starBtn.style.display = "none";
    backArrow.style.display = "none";
}

function checksForEncounter() {
    // gets random number between 1 and 500
   var enemySpawnChance = Math.floor(Math.random() * 500) + 1;
   if(enemySpawnChance === 500)
   {
     playersLastPosX = player.x;
     playerLastPosY = player.y;
     battle();
   }
}

function battle()
{
    slimeShouldBeOnScreen = true;
    backgroundImg.src = "";

    player.x = 410;
    player.y = 500;
    playerCanMove = false;

    playersHealthBar.style.display = "block";
    enemiesHealthBar.style.display = "block";
    attackBtn.style.display = "block";
}

function attacks()
{
    enemiesHealth = enemiesHealth - 10;
    enemieHealthScale = enemieHealthScale - 2;
    enemiesHealthBar.style.width = enemieHealthScale + "%";
    console.log(enemiesHealth);
    if (enemiesHealth === 0)
    {
        var hitDropChance = Math.floor(Math.random() * 3) + 1;
        if(hitDropChance === 1)
        {
            playerShouldBeOnScreen = false;
            backgroundImg.src = "assets/img/hint.png";
            backArrow.style.display = "block";
        }
        else
        {
            insideCave();
        }
        slimeShouldBeOnScreen = false;
        attackBtn.style.display = "none";
        playersHealthBar.style.display = "none";
        enemiesHealthBar.style.display = "none";
        player.x = playersLastPosX;
        player.y = playerLastPosY;

        enemieHealthScale = 20;
        enemiesHealth = 100
        enemiesHealthBar.style.width = enemieHealthScale + "%";

        playersHealthScale = 20;
        playersHealth = 100;
        playersHealthBar.style.width = playersHealthScale + "%";
    }

}